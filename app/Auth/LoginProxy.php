<?php

namespace App\Auth;

use DB;
use Route;

class LoginProxy
{
    public function attemptLogin($email, $password)
    {
        $client = DB::table('oauth_clients')->where('password_client', true)->first();
        request()->request->add([
            'grant_type'    => 'password',
            'client_id'     => $client->id,
            'client_secret' => $client->secret,
            'scope'         => '*',
            'username'      => $email,
            'password'      => $password,
        ]);

        $response = Route::dispatch(request()->create('/oauth/token', 'POST'));

        return response()->json(json_decode($response->getContent(), true), $response->getStatusCode());
    }
}
