<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $fillable=['bldg_name','floor','unite_no','bldg_no','street_one','street_two','district',
        'city','customer_name','customer_email','bill_date'];
}
