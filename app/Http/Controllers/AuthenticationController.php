<?php

namespace App\Http\Controllers;

use App\Auth\LoginProxy;
use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;


class AuthenticationController extends Controller
{
    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        return $this->loginProxy->attemptLogin($email, $password);
    }

    public function register(Request $request)
    {

        $request->validate([
            'email' => 'required|unique:users,email',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required|confirmed|regex:/(?=^.{8,12}$)(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[\W_])^.*/',
            'password_confirmation' => 'required',
        ],[
            'password.regex' => '1sS@kl[]32423'

        ]);

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();



        return response(['token' => $user->createToken('Something')->accessToken, 'user' => $user]);

    }

}
