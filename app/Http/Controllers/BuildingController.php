<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Building;

class BuildingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bill = Building::all();
        return response()->json($bill, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bill = Building::create($request->all());
        $bill->save();
        return response()->json($bill, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bill = Building::findOrFail($id);

        return $bill;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, $id)
//    {
//        $bill = Building::findOrFail($id);
//        $bill->update($request->all());
//        return response()->json($bill, 200);
//    }
    public function update(Request $request)
    {

        $records = $request->all();
        $results = $errors = [];
        foreach ($records as $record) {
            if (isset($record['id'])) {
                $serve = Building::findOrFail($record['id']);
                unset($record['id']);
                array_filter($record);
                $results[] = tap($serve)->update($record);
            } else {
                $errors[] = $record;
            }
        }
        $results = ['data' => $results];
        if (count($errors)) {
            $results = array_merge($results, [
                    'errors' => ['message' => 'no id found for these records', 'records' => $errors]
                ]
            );
        }
        return response()->json($results, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bill = Building::findOrFail($id);
        $bill->delete($id);
        return response()->json($bill, 204);
    }
}
