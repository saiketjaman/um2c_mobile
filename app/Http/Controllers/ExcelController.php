<?php

namespace App\Http\Controllers;


use App\Building;
use App\Parking;
use App\Water;
use App\Electricity;
use App\Http\Requests\ExcelRequest;
use Rap2hpoutre\FastExcel\FastExcel;
//use Illuminate\Support\Facades\Storage;

class ExcelController extends Controller
{

    public function index()
    {
        /*
         * this is for download and view file from dropbox cloud
         * return $excel= Storage::disk('dropbox')->get('file.txt');
         * return Storage::disk('dropbox')->download('file.txt');
        */

        return "WELCOME TO UM2C MOBILE API ";
    }

    public function readexcelfile()
    {
        return $collection = (new FastExcel)->import('demofile.xlsx');

    }
    public function saveexcelfile(){

    }
    public function downloadbuilding(ExcelRequest $request)
    {

        return (new FastExcel(Building::all()))->download('Building.xlsx');

    }
    public function downloadwater(ExcelRequest $request)
    {

        return (new FastExcel(Water::all()))->download('Water.xlsx');

    }
    public function downloadelectricity(ExcelRequest $request)
    {

        return (new FastExcel(Electricity::all()))->download('Electricity.xlsx');


    }
    public function downloadparking(ExcelRequest $request)
    {

        return (new FastExcel(Parking::all()))->download('Parking.xlsx');

    }
}
