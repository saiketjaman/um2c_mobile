<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parking;


class ParkingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $park = Parking::all();
        return response()->json($park, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $park = Parking::create($request->all());
        $park->save();
        return response()->json($park, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $park = Parking::findOrFail($id);

        return $park;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $park = Parking::findOrFail($id);
        $park->update($request->all());
        return response()->json($park, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $park = Parking::findOrFail($id);
        $park->delete($id);
        return response()->json($park, 204);
    }
}
