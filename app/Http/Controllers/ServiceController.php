<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Water;


class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $serve = Water::all();
        return response()->json($serve, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serve = Water::create($request->all());
        $serve->save();
        return response()->json($serve, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $serve = Water::findOrFail($id);

        return $serve;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, $id)
//    {
//
//        $serve = Water::findOrFail($id);
//        $serve->update($request->all());
//        return response()->json($serve, 200);
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $records = $request->all();
        $results = $errors = [];

        foreach ($records as $record) {
            if (isset($record['id'])) {
                $serve = Water::findOrFail($record['id']);
                unset($record['id']);
                array_filter($record);
                $results[] = tap($serve)->update($record);
            } else {
                $errors[] = $record;
            }
        }
        $results = ['data' => $results];

        if (count($errors)) {
            $results = array_merge($results, [
                    'errors' => ['message' => 'no id found for these records', 'records' => $errors]
                ]
            );
        }
        return response()->json($results, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $serve = Water::findOrFail($id);
        $serve->delete($id);
        return response()->json($serve, 204);
    }
}
