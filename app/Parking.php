<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parking extends Model
{
    protected $fillable =['contract_no','service_type','month_name','year','area','car_park'];
}
