<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Water extends Model
{
    protected $fillable =['contract_no','service_type','device_type','serial_no','schedule_mr_date','bill_period_start',
        'bill_period_end','mrr_no','system_status','previous_mr_date','previous_mr_time','previous_mr','current_mr_date','current_mr_time','current_mr','mr_type','mr_error'];

}
