<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bldg_name');
            $table->string('floor');
            $table->string('unit_no');
            $table->integer('bldg_no')->unsigned();
            $table->string('street_one');
            $table->string('street_two');
            $table->string('district');
            $table->string('city');
            $table->string('customer_name')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('bill_date')->nullable();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buildings');
    }
}
