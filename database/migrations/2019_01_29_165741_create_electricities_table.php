<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectricitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electricities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contract_no');
            $table->string('service_type');
            $table->string('device_type')->nullable();
            $table->string('serial_no')->nullable();
            $table->string('schedule_mr_date');
            $table->string('bill_period_start');
            $table->string('bill_period_end');
            $table->string('mrr_no')->nullable();
            $table->string('system_status')->nullable();
            $table->string('previous_mr_date')->nullable();
            $table->time('previous_mr_time')->nullable();
            $table->decimal('previous_mr',8,5)->nullable();
            $table->string('current_mr_date')->nullable();
            $table->time('current_mr_time')->nullable();
            $table->decimal('current_mr',8,5)->nullable();
            $table->string('mr_type')->nullable();
            $table->string('mr_error')->nullable();
            $table->softDeletes();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electricities');
    }
}
