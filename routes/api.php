<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login','AuthenticationController@login');
Route::post('/register','AuthenticationController@register');

Route::middleware(['auth:api'])->group(function () {

    // for billing
    Route::get('building', 'BuildingController@index');
    Route::get('building/{id}', 'BuildingController@show');
    Route::post('building', 'BuildingController@store');
    Route::put('building', 'BuildingController@update');
    Route::delete('building/{id}', 'BuildingController@destroy');

// for electricity
    Route::get('electricity', 'ElectricityController@index');
    Route::get('electricity/{id}', 'ElectricityController@show');
    Route::post('electricity', 'ElectricityController@store');
    Route::put('electricity', 'ElectricityController@update');
    Route::delete('electricity/{id}', 'ElectricityController@destroy');
// for water
    Route::get('water', 'ServiceController@index');
    Route::get('water/{id}', 'ServiceController@show');
    Route::post('water', 'ServiceController@store');
    Route::put('water', 'ServiceController@update');
    Route::delete('water/{id}', 'ServiceController@destroy');
});

// for forgot password reset

Route::group([
    'namespace' => 'Auth',
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});