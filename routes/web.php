<?php

/**
 * 1. (api) when excel file is uploaded you process data and you store in you db. (you want to store to onedrive)
 * 2. you need an api where then can download the excel file
 * 3. you need an api where the can see the db data as json
 */

//Route::get('/','ExcelController@index');


Route::get('/',['uses'=>'ExcelController@index','as'=>'excel.read']);
//Route::post('/address',['uses'=>'ExcelController@addressfile','as'=>'excel.address']);
Route::get('/readfile',['uses'=>'ExcelController@readexcelfile','as'=>'excel.readfile']);
//Route::get('/savefile',['uses'=>'ExcelController@saveexcelfile','as'=>'excel.savefile']);
Route::get('/building',['uses'=>'ExcelController@downloadbuilding','as'=>'excel.building']);
Route::get('/electricity',['uses'=>'ExcelController@downloadelectricity','as'=>'excel.electricity']);
Route::get('/water',['uses'=>'ExcelController@downloadwater','as'=>'excel.water']);
Route::get('/parking',['uses'=>'ExcelController@downloadparking','as'=>'excel.parking']);
//Route::get('/readfile',['uses'=>'ExcelController@readfile','as'=>'excel.readfile']);

Route::post('/login', 'AuthenticationController@login');
